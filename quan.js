//var baseUrl = "http://localhost:8080/tbk/";
//var baseUrl = "http://192.168.137.1:8080/tbk/";
var baseUrl = "https://q.hayye.cn/tbk/";

$(function () {
    $("#load").hide();
    $("#copyTips").hide();
    $('.ui-searchbar').click(function(){
        $('.ui-searchbar-wrap').addClass('focus');
        $('.ui-searchbar-input input').focus();
    });
    $("#clear").click(function () {
        $('.ui-searchbar-input input').val("");
    });
    scrollListener();

    $("#tpwdError button").click(function () {
       $("#tpwdError").hide();
    });
});
var num = 0;
var page = 0;
var size = 15;
var finish = 0;
var key;
function search() {
    $("#list").html("");
    $("#load").show();
    key = $("#key").val();
    load();
}
function load() {
    var param = {
        key:key,
        page:page,
        size:size,
        hasCoupon: true
    };
    var url= baseUrl+'q/searchMaterials';
    $.post(url,param,function(result){
        $("#load").hide();
        if (result.code == 50000) {
            var itemList = result.data.list;
            var _html = '';
            if (itemList.length>0){
                $.each(itemList,function (index,item) {
                    _html += ' <li>' +
                        '<div class="ui-list-img-square">' +
                        '    <span style="background-image:url('+item.img+')"></span>\n' +
                        '</div>' +
                        '<div class="ui-list-info ui-border-t">' +
                        '    <h4 class="ui-nowrap"><a href="'+item.couponUrl+'">'+item.title+'</a></h4>' +
                        '    <p class="ui-nowrap" style="color: #999999;font-size: 12px;"><span style="">'+item.shopTitle+'</span></p>' +
                        '    <p class="ui-nowrap" style="font-weight: bold;font-size: 16px;">折扣价：<span style="color: red">￥'+ item.zkPrice +'</span></p>' +
                        '    <p class="ui-nowrap">' +
                        '       <span style="background-color: red;color: white;font-weight: bold;">券 ￥'+item.couponAmount+'</span>' +
                        '       <button id="btn_'+num+'" class="ui-btn ui-btn-danger" style="margin-left: 30px;height: unset;line-height: unset;display: unset;" onclick="getTpwd('+num+','+item.itemId+')">淘口令</button>' +
                        '    </p>' +
                        '</div>' +
                        '</li>';
                    num++;
                });
                $("#list").append(_html);
                finish = 0;
            } else {
                finish = 1;
                $("#list").append("没有更多数据了...")
            }
        }else {
            $("#list").html("");
            $("#errorTip").show();
        }
    });
}
//复制淘口令
function copyTpwd(btnId) {
    var clipboard = new Clipboard(btnId);
    clipboard.on('success', function (e) {
        console.log(e)
        console.log(JSON.stringify(e))
        $("#tpwdTips").show();
        setTimeout(function () {
            $("#tpwdTips").hide();
        }, 1500);
    });
    clipboard.on('error',function(e) {
        //alert(JSON.stringify(e))
        $("#tpwd_value").html($(btnId).attr("data-clipboard-text"));
        $("#tpwdError").show();
    });
}
function getTpwd(num,itemId) {
    var btnId = "#btn_"+num;
    var tpwd = $(btnId).attr("data-clipboard-text");
    if(tpwd){
        copyTpwd(btnId);
    }else {
        $("#pageLoad").show();
        var url = baseUrl + 'q/getTpwd';
        var param = {
            itemId: itemId
        };
        $.ajax({
            url: url,
            type: 'post',
            data: param,
            dataType: 'json',
            async: false,
            success: function (result) {
                $("#pageLoad").hide();
                if (result.code == 50000) {
                    tpwd = result.data.toString();
                    $(btnId).attr("data-clipboard-text", tpwd);
                    copyTpwd(btnId);
                } else {
                    $("#pageLoadError").show();
                    setTimeout(function () {
                        $("#pageLoadError").hide();
                    }, 1500);
                }
            }
        });
    }
}
//滚动监听
function scrollListener(){
    $(window).scroll(function() {
        if (finish == 0){
            var docHeight=$(document).height();
            var docScrollTop=$(document).scrollTop();
            var windowHeight;
            if($(document).height()>=$(window).height()){
                windowHeight=document.body.clientHeight;
            }else{
                windowHeight=$(document).height();
            };
            if (docScrollTop >= docHeight - windowHeight) {
                finish = 1;
                page+=1;
                load();
            }
        }
    });
};